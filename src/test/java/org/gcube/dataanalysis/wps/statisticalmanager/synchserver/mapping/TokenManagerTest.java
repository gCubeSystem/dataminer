package org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mapping;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

public class TokenManagerTest {

	private static final String JWT_TOKEN = "";
	private static final String SCOPE = "/gcube/devsec/devVRE";

	
	@Test
	public void retrieveTokenFromUriResolver() throws Exception {
		try {
			BasicConfigurator.configure();
			System.out.println("Test Retrieve Token From Uri Resolver");
			TokenManager tm = new TokenManager();
			String token = tm.getGcubeTokenFromUriResolver(JWT_TOKEN, SCOPE);
			System.out.println("GcubeToken retrieved: "+token);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.getStackTrace();
		}
	}

}
