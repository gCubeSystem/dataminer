package org.gcube.dataanalysis.wps.statisticalmanager.synchserver.mapping;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.ws.rs.core.Response;

import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.dataanalysis.wps.statisticalmanager.synchserver.is.InformationSystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TokenManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenManager.class);

	String username;
	String scope;
	String token;
	String tokenQualifier;

	public String getScope() {
		return scope;
	}

	public String getUserName() {
		return username;
	}

	public String getToken() {
		return token;
	}

	public String getTokenQualifier() {
		return tokenQualifier;
	}

	public void getCredentials() {
		try {
			LOGGER.info("Retrieving token credentials");
			// get username from SmartGears
			username = AuthorizationProvider.instance.get().getClient().getId();
			token = SecurityTokenProvider.instance.get();
			if (token == null || token.isEmpty()) {
				String jwtToken = AccessTokenProvider.instance.get();
				scope = ScopeProvider.instance.get();
				token = getGcubeTokenFromUriResolver(jwtToken, scope);
			}
			AuthorizationEntry entry = authorizationService().get(token);
			scope = entry.getContext();
			tokenQualifier = entry.getQualifier();
		} catch (Exception e) {
			LOGGER.error("Error Retrieving token credentials: "+e.getLocalizedMessage(),e);
			scope = null;
			username = null;

		}
		if ((scope == null || username == null) && ConfigurationManager.isSimulationMode()) {
			scope = ConfigurationManager.defaultScope;
			username = ConfigurationManager.defaultUsername;
		}
		LOGGER.info("Retrieved scope: {} Username: {} Token {} SIMULATION MODE: {} ", scope, username, token,
				ConfigurationManager.isSimulationMode());

	}

	public String getGcubeTokenFromUriResolver(String jwtToken, String scope) throws Exception {
		String gcubeToken = null;
		String uriResolverOatURL = InformationSystemUtils.retrieveUriResolverOat(scope);
		try {
			LOGGER.info("Create Request: "+ uriResolverOatURL);
			URL urlObj = new URL(uriResolverOatURL);
			HttpURLConnection connection = (HttpURLConnection) urlObj.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "Bearer " + jwtToken);
			connection.setDoOutput(true);
			try (AutoCloseable conc = () -> connection.disconnect()) {
				int responseCode = connection.getResponseCode();
				LOGGER.info("Response Code: " + responseCode);
				
				if (Response.Status.fromStatusCode(responseCode).compareTo(Response.Status.OK) == 0) {
					try (InputStream ins = connection.getInputStream();
							BufferedReader in = new BufferedReader(new InputStreamReader(ins))) {
						String inputLine = null;
						while ((inputLine = in.readLine()) != null) {
							break;
						}
						gcubeToken = inputLine;
					}
				} else {
					String error = "Invalid Response Code retrieving GCube Token from Uri Resolver: " + responseCode;
					LOGGER.error(error);
					try (InputStream ins = connection.getErrorStream();
							BufferedReader in = new BufferedReader(new InputStreamReader(ins))) {
						String inputLine = null;
						while ((inputLine = in.readLine()) != null) {
							LOGGER.error(inputLine);
						}
					}
					throw new Exception(error);
				}
			}

		} catch (IOException e) {
			LOGGER.error("Error retrieving GcubeToken from Uri Resolver: "+e.getLocalizedMessage());
			e.printStackTrace();
			throw e;
		}
		LOGGER.info("Retrieved GcubeToken: "+gcubeToken);
		return gcubeToken;
	}

}
