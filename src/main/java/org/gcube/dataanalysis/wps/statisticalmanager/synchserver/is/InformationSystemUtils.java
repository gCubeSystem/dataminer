package org.gcube.dataanalysis.wps.statisticalmanager.synchserver.is;

import java.util.List;

import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Runtime;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InformationSystemUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(InformationSystemUtils.class);
	private static final String URI_RESOLVER_SERVICE_CATEGORY = "Service";
	private static final String URI_RESOLVER_SERVICE_NAME = "HTTP-URI-Resolver";

	public static String retrieveUriResolverOat(String scope) throws Exception {
		try {
			LOGGER.info("Retrieve URI Resolver Oat Service Info");

			if (scope == null || scope.length() == 0) {
				String error="Invalid request scope: " + scope;
				LOGGER.error(error);
				throw new Exception(error);
			}

			ScopeProvider.instance.set(scope);

			SimpleQuery query = ICFactory.queryFor(ServiceEndpoint.class);
			query.addCondition("$resource/Profile/Category/text() eq '" + URI_RESOLVER_SERVICE_CATEGORY + "'")
					.addCondition("$resource/Profile/Name/text() eq '" + URI_RESOLVER_SERVICE_NAME + "'")
					.setResult("$resource/Profile/RunTime");
			DiscoveryClient<Runtime> client = ICFactory.clientFor(Runtime.class);
		
			List<Runtime> runtimeList = client.submit(query);
			String serviceAddress = null;
			if (runtimeList != null && !runtimeList.isEmpty()) {
				for (int i = 0; i < runtimeList.size(); i++) {
					Runtime accessPoint = runtimeList.get(i);
					if (accessPoint != null) {
						StringBuilder sb=new StringBuilder();
						sb.append("https://");
						sb.append(accessPoint.hostedOn());
						sb.append("/oat/get");
						serviceAddress=sb.toString();
						break;
					}
				}
			} else {
				String error="RuntimeList error: "+runtimeList;
				LOGGER.error(error);
				throw new Exception(error);
			}

			LOGGER.info("Uri Resolver Oat Service Info: " + serviceAddress);
			return serviceAddress;

		} catch (Throwable e) {
			LOGGER.error("Error in discovery Uri Resolver Oat Service Endpoint in scope: " + scope);
			LOGGER.error(e.getLocalizedMessage(),e);
			throw e;
		}
	}
}
