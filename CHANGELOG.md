This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "dataminer"


## [v1.9.1]

- Updated gcube-bom to 2.4.1


## [v1.9.0] - 2022-04-05

-  Added support to  new JWT token via URI Resolver [#23107]


## [v1.8.1] - 2022-03-21

- Update wps service to support not writing of the computation status to the user's workspace [#23054]
- Fixed protocol parameter when persistence is disabled


## [v1.8.0] - 2022-01-24

- Fixed max computations support [#22700]


## [v1.7.1] - 2021-05-24

- Fixed obsolete short urls [#20971]


## [v1.7.0] - 2020-11-20

- import range modified to resolve old repositories invalid url


## [v1.6.0] - 2020-05-12

- Added storagehub retry in InputsManager class, getLocalFile method [#19253]


## [v1.5.9] - 2019-11-20

- Fixed Content-Type support for files in the results of computations [#18096]


## [v1.5.8] - 2019-10-01

- Fixed https link for output parameter [#17659]


## [v1.5.7] - 2019-03-01

- Updated https support [#13024]



## [v1.5.2] - 2017-12-13

- added the right extension on output file
- lock file created on execution


## [v1.5.1] - 2017-09-14

- added accounting on algorithm execution


## [v1.5.0] - 2017-07-31

- service interface classes moved to wps project


## [v1.1.0] - 2016-10-03

- First Release

